import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:phase_gaming/src/assets/style/phase_gaming_theme.dart';
import 'package:phase_gaming/src/blocs/app/app_bloc.dart';
import 'package:phase_gaming/src/blocs/app/app_state.dart';
import 'package:phase_gaming/src/ui/navigation/app_route.dart';

void main() {
  _setupLogging();
  // showLayoutGuidelines();
  runApp(PhaseGaming());
}

void showLayoutGuidelines() {
  debugPaintSizeEnabled = true;
}

void _setupLogging() {
  Logger.root.level = Level.INFO;
  Logger.root.onRecord.listen((rec) {
    print('RoraLog - ${rec.level.name}: ${rec.time}: ${rec.message}');
  });
}

class PhaseGaming extends StatefulWidget {
  const PhaseGaming({Key? key}) : super(key: key);

  @override
  _PhaseGamingState createState() => _PhaseGamingState();
}

class _PhaseGamingState extends State<PhaseGaming> {
  late AppRoute _appRouter;

  @override
  void initState() {
    // TODO: implement initState
    _appRouter = AppRoute();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => AppBloc(AppState()),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: PhaseGamingTheme.dark(),
          home: Router(
            routerDelegate: _appRouter,
          ),
        ));
  }
}
