import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:phase_gaming/src/ui/navigation/app_route.dart';
import 'package:phase_gaming/src/ui/screen/home/home_screen.dart';
import 'package:phase_gaming/src/ui/screen/library/library_screen.dart';
import 'package:phase_gaming/src/ui/screen/search/search_screen.dart';
import 'package:phase_gaming/src/ui/screen/settings/settings_screen.dart';

import 'blocs/app/app_bloc.dart';
import 'blocs/app/app_event.dart';

class App extends StatefulWidget {
  static MaterialPage page(int currentTab) {
    return MaterialPage(
        name: AppPath.homePath,
        key: ValueKey(AppPath.homePath),
        child: App(
          currentTab: currentTab,
        ));
  }

  final int currentTab;
  App({required this.currentTab});

  @override
  State<StatefulWidget> createState() => _AppState();
}

class _AppState extends State<App> {
  List<Widget> pages = <Widget>[
    HomeScreen(),
    SearchScreen(),
    LibraryScreen(),
    SettingsScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          body: IndexedStack(
            index: widget.currentTab,
            children: pages,
          ),
          bottomNavigationBar: Container(
            decoration: BoxDecoration(
                border: Border(top: BorderSide(color: Colors.white))),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              selectedItemColor:
                  Theme.of(context).textSelectionTheme.selectionColor,
              onTap: (index) {
                BlocProvider.of<AppBloc>(context)
                    .add(GotoTabEvent(currentTab: index));
              },
              currentIndex: widget.currentTab,
              items: [
                BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.search), label: 'Search'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.games), label: 'Library'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.settings), label: 'Settings'),
              ],
            ),
          ),
        ),
        onWillPop: () async => false);
  }
}
