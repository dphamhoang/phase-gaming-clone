import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';

class PhaseGamingTheme {
  static TextTheme lightTextTheme = TextTheme(
      bodyText1: GoogleFonts.openSans(
          fontSize: 14, fontWeight: FontWeight.w700, color: Colors.black),
      headline1: GoogleFonts.openSans(
          fontSize: 40,
          fontWeight: FontWeight.bold,
          color: AppColors.colorPrimary),
      headline2: GoogleFonts.openSans(
          fontSize: 21,
          fontWeight: FontWeight.w700,
          color: AppColors.colorPrimary),
      headline3: GoogleFonts.openSans(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: AppColors.colorPrimary));

  static TextTheme darkTextTheme = TextTheme(
      bodyText1: GoogleFonts.openSans(
          fontSize: 14, fontWeight: FontWeight.w700, color: Colors.white),
      bodyText2: GoogleFonts.openSans(
          fontSize: 12, color: Colors.white, fontWeight: FontWeight.w600),
      headline1: GoogleFonts.openSans(
        fontSize: 50,
        fontWeight: FontWeight.w600,
        color: AppColors.colorPrimary,
      ),
      headline2: GoogleFonts.openSans(
          fontSize: 21, fontWeight: FontWeight.w700, color: Colors.white),
      headline3: GoogleFonts.openSans(
          fontSize: 14, fontWeight: FontWeight.w600, color: Colors.white),
      caption: GoogleFonts.openSans(
          fontSize: 12,
          fontWeight: FontWeight.normal,
          color: AppColors.colorPrimary));

  static dark() {
    return ThemeData.dark().copyWith(
        scaffoldBackgroundColor: AppColors.colorPrimaryDark,
        primaryColor: HexColor('#1D1D20'),
        accentColor: HexColor('#2D2E2E'),
        textSelectionTheme:
            TextSelectionThemeData(selectionColor: HexColor('#06bbf9')),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
            backgroundColor: AppColors.colorPrimaryDark,
            selectedItemColor: AppColors.colorPrimary,
            unselectedItemColor: Colors.white,
            selectedIconTheme: IconThemeData(color: AppColors.colorPrimary)),
        textTheme: darkTextTheme);
  }
}
