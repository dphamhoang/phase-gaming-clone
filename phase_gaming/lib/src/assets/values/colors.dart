import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  static const Color colorPrimary = Color.fromARGB(255, 6, 187, 249);
  static const Color colorPrimaryDark = Color.fromARGB(255, 29, 29, 32);
  static const Color colorAccent = Color.fromARGB(255, 45, 46, 46);

  static const Color textColor = Colors.white;
}
