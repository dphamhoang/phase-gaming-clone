class Dimens {
  // SPACE
  static const double MINN_SPACE = 3;
  static const double MINNN_SPACE = 1;
  static const double MIN_SPACE = 5;
  static const double MIN_MEDIUM_SPACE = 8;
  static const double MEDIUM_SPACE = 10;
  static const double NORMAL_SPACE = 16;
  static const double MAX_SPACE = 20;
  static const double MAXX_SPACE = 28;
  static const double MAXXX_SPACE = 35;
  static const double MAXXXX_SPACE = 45;

  // RADIUS
  static const double MIN_RADIUS = 2;
  static const double NORMAL_RADIUS = 6;
  static const double MEDIUM_RADIUS = 8;
  static const double MAX_RADIUS = 10;
  static const double MAXX_RADIUS = 16;

  // TEXT SIZE
  static const double LOGO_TEXT_SIZE = 35;
  static const double MAX_TEXT_SIZE = 30;
  static const double HEADER_TEXT_SIZE = 20;
  static const double TITLE_TEXT_SIZE = 18;
  static const double MAX_SUB_TITLE_TEXT_SIZE = 16;
  static const double SUB_TITLE_TEXT_SIZE = 14;
  static const double NORMAL_TEXT_SIZE = 12;
  static const double MIN_TEXT_SIZE = 11;
  static const double MINN_TEXT_SIZE = 8;
  static const double MINNN_TEXT_SIZE = 5;
  static const double MINNNN_TEXT_SIZE = 3;

  //ICON
  static const double IC_TOUCHABLE_SIZE = 48;
  static const double IC_MEDIUM_SIZE = 38;
  static const double IC_MIN_SIZE = 20;
  static const double IC_MINN_SIZE = 15;

  static const double BANNER_HEIGHT = 200;
  static const double GAME_ITEM_HEIGHT = 150;
  static const double GAME_MIN_INFO_ITEM_HEIGHT = 140;
  static const double GAME_INFO_ITEM_HEIGHT = 240;
  static const double GAME_INFO_IMAGE_LAYER_HEIGHT = 160;
  static const double CATEGORY_HEIGHT = 32;
  static const double VERTICAL_GAME_ITEM_HEIGHT = 100;
  static const double NORMAL_WIDTH_IMG = 215;
  static const double SMALL_WIDTH_IMG = 181;
  static const double ROUNDED_RADIUS = 250;
}
