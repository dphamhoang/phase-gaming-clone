import 'package:bloc/bloc.dart';
import 'package:phase_gaming/src/blocs/app/app_event.dart';
import 'package:phase_gaming/src/blocs/app/app_state.dart';
import 'package:phase_gaming/src/resources/repository/auth_repository.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc(AppState initialState) : super(initialState) {
    on<InitializeEvent>(initializeApp);
    on<LoggedInEvent>(isUserLogged);
    on<GuestEvent>(guestEvent);
    on<GotoTabEvent>(gotoTab);
    on<GameDetailEvent>(goToGameDetail);
  }

  final authRepository = AuthRepository();
  void initializeApp(InitializeEvent event, Emitter<AppState> emit) {
    emit(state.copyWith(isInitializeApp: true));
  }

  void isUserLogged(LoggedInEvent event, Emitter<AppState> emit) async {
    final result = await authRepository.isLoggedIn();
    if (result) {
      emit(state.copyWith(isLoggedIn: result));
    }
  }

  void guestEvent(GuestEvent event, Emitter<AppState> emit) async {
    emit(state.copyWith(isGuest: true));
  }

  void gotoTab(GotoTabEvent event, Emitter<AppState> emit) async {
    emit(state.copyWith(currentTab: event.currentTab));
  }

  void goToGameDetail(GameDetailEvent event, Emitter<AppState> emit) {
    emit(state.copyWith(gameId: event.id));
  }
}
