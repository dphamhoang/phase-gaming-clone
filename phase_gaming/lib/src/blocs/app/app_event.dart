abstract class AppEvent {}

class InitializeEvent extends AppEvent {}

class LoggedInEvent extends AppEvent {}

class GuestEvent extends AppEvent {
  final bool isGuest;
  GuestEvent({required this.isGuest});
}

class GotoTabEvent extends AppEvent {
  final int currentTab;
  GotoTabEvent({required this.currentTab});
}

class GameDetailEvent extends AppEvent {
  int id;
  GameDetailEvent({required this.id});
}
