class AppState {
  bool initializeApp;
  bool isLoggedIn;
  bool darkTheme;
  bool isGuest;
  int currentTab;
  bool isLoading;
  int gameId;
  AppState(
      {this.initializeApp = false,
      this.isLoggedIn = false,
      this.isGuest = false,
      this.darkTheme = true,
      this.currentTab = 0,
      this.isLoading = false,
      this.gameId = 0});
  AppState copyWith(
      {bool? isInitializeApp, bool? isLoggedIn, bool? darkTheme, bool? isGuest, int? currentTab, int? gameId}) {
    return AppState(
        initializeApp: isInitializeApp ?? this.initializeApp,
        isLoggedIn: isLoggedIn ?? this.isLoggedIn,
        darkTheme: darkTheme ?? this.darkTheme,
        isGuest: isGuest ?? this.isGuest,
        gameId: gameId ?? 0,
        currentTab: currentTab ?? 0);
  }
}
