import 'package:bloc/bloc.dart';
import 'package:phase_gaming/src/blocs/game/game_event.dart';
import 'package:phase_gaming/src/blocs/game/game_state.dart';
import 'package:phase_gaming/src/resources/repository/game_repository.dart';

class GameBloc extends Bloc<GameEvent, GameState> {
  GameBloc(GameState initialState) : super(initialState) {
    on<GetGameById>(getGameById);
  }
  final gameRepository = GameRepository();

  Future<void> getNewGameList(GetNewGameEvent event, Emitter<GameState> emit) async {
    final newGames = await gameRepository.getNewGame(event.page, event.pageSize);
  }

  Future<void> getGameById(GetGameById event, Emitter<GameState> emit) async {
    emit(state.copyWith(isLoading: true));
    final game = await gameRepository.getGame(event.id);
    if (game.body != null) {
      emit(state.copyWith(isLoading: false, game: game.body));
    }
  }
}
