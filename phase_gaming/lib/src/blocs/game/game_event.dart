abstract class GameEvent {
  int page, pageSize;
  GameEvent({this.page = 1, this.pageSize = 10});
}

class GetNewGameEvent extends GameEvent {}

class GetHotGameEvent extends GameEvent {}

class GetTrendingGameEvent extends GameEvent {}

class GetGameById extends GameEvent {
  int id;
  GetGameById({required this.id});
}
