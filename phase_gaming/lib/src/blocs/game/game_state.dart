import 'package:phase_gaming/src/models/game.dart';

class GameState {
  Game? game;
  final bool isLoading;
  GameState({this.game, this.isLoading = false});
  GameState copyWith({Game? game, required bool isLoading}) =>
      GameState(game: game ?? this.game, isLoading: isLoading);
}
