import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/blocs/home/home_event.dart';
import 'package:phase_gaming/src/blocs/home/home_state.dart';
import 'package:phase_gaming/src/models/banner.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/models/tag.dart';
import 'package:phase_gaming/src/resources/constant/constant.dart';
import 'package:phase_gaming/src/resources/repository/banner_repository.dart';
import 'package:phase_gaming/src/resources/repository/game_repository.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(HomeState initialState) : super(initialState) {
    on<GetHomeData>(getHomeData);
  }

  final gameRepository = GameRepository();
  final bannerRepository = BannerRepository();

  Future<void> getHomeData(GetHomeData event, Emitter<HomeState> emit) async {
    emit(state.copyWith(isLoading: true));
    final banner = await getBanner();
    final newGames = await getNewGame();
    final trendingGames = await getTrendingGames();
    final hotGames = await getHotGames();
    final editorChoice = await getEditorChoice();
    final tags = await getTags();

    if (banner.body!.length > 0 &&
        newGames.body!.length > 0 &&
        trendingGames.body!.length > 0 &&
        hotGames.body!.length > 0 &&
        editorChoice.body!.length > 0 &&
        tags.body!.length > 0) {
      emit(state.copyWith(
          isLoading: false,
          banners: banner.body,
          newGame: newGames.body,
          hotGame: hotGames.body,
          trendingGame: trendingGames.body,
          editorChoice: editorChoice.body,
          tags: tags.body));
    }
  }

  Future<Response<List<AppBanner>>> getBanner() async {
    final banners = await bannerRepository.getBanner();
    return banners;
  }

  Future<Response<List<Game>>> getNewGame() async {
    final newGame = await gameRepository.getNewGame(1, Constant.pageSize);
    return newGame;
  }

  Future<Response<List<Game>>> getTrendingGames() async {
    final trendingGames =
        await gameRepository.getTrendingGame(1, Constant.pageSize);
    return trendingGames;
  }

  Future<Response<List<Game>>> getHotGames() async {
    final hotGames = await gameRepository.getHotGame(1, Constant.pageSize);
    return hotGames;
  }

  Future<Response<List<Game>>> getEditorChoice() async {
    final editorChoices =
        await gameRepository.getEditorChoice(1, Constant.pageSize);
    return editorChoices;
  }

  Future<Response<List<Tag>>> getTags() async {
    final tags = await gameRepository.getTags();
    return tags;
  }
}
