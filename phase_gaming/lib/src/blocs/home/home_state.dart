import 'package:phase_gaming/src/models/banner.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/models/tag.dart';

class HomeState {
  final List<Game> hotGame;
  final List<Game> trendingGame;
  final List<Game> newGame;
  final List<Game> editorChoice;

  final List<AppBanner> banners;

  final List<Tag> tags;

  final bool isLoading;
  HomeState(
      {this.banners = const <AppBanner>[],
      this.newGame = const <Game>[],
      this.trendingGame = const <Game>[],
      this.hotGame = const <Game>[],
      this.editorChoice = const <Game>[],
      this.tags = const <Tag>[],
      this.isLoading = false});

  HomeState copyWith(
          {List<Game>? newGame,
          List<Game>? trendingGame,
          List<Game>? hotGame,
          List<Game>? editorChoice,
          List<AppBanner>? banners,
          List<Tag>? tags,
          required bool isLoading}) =>
      HomeState(
          newGame: newGame ?? this.newGame,
          trendingGame: trendingGame ?? this.trendingGame,
          hotGame: hotGame ?? this.hotGame,
          editorChoice: editorChoice ?? this.editorChoice,
          banners: banners ?? this.banners,
          tags: tags ?? this.tags,
          isLoading: isLoading);
}
