import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:phase_gaming/src/blocs/login/login_event.dart';
import 'package:phase_gaming/src/blocs/login/login_state.dart';
import 'package:phase_gaming/src/resources/repository/auth_repository.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final _authRepository = AuthRepository();
  LoginBloc(LoginState initialState) : super(initialState) {
    on<LoginSubmitted>(login);
  }

  void login(LoginSubmitted event, Emitter<LoginState> emit) async {
    emit(state.copyWith(
        email: event.email, password: event.password, isSubmitting: true));
    final result = await _authRepository.login(event.email, event.password);

    if (result.statusCode != 200) {
      final errMsg = json.decode(result.error.toString());
      emit(state.copyWith(isSubmitting: false, errMsg: errMsg['message']));
    } else {
      emit(state.copyWith(isSubmitting: false, isSuccess: true));
    }
  }
}
