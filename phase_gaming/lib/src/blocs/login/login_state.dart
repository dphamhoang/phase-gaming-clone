class LoginState {
  String email;
  String password;
  bool isSubmitting;
  bool isSuccess;
  String errMsg;
  LoginState(
      {this.email = '',
      this.password = '',
      this.isSubmitting = false,
      this.isSuccess = false,
      this.errMsg = ''});
  LoginState copyWith(
      {String? email,
      String? password,
      required bool isSubmitting,
      bool? isSuccess,
      String? errMsg}) {
    return LoginState(
        email: email ?? this.email,
        password: password ?? this.password,
        isSubmitting: isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess,
        errMsg: errMsg ?? this.errMsg);
  }
}
