import 'package:json_annotation/json_annotation.dart';
import 'package:phase_gaming/src/models/user.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse {
  @JsonKey(name: 'info')
  User info;
  @JsonKey(name: 'token')
  String token;
  LoginResponse({required this.info, required this.token});
  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);
  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}
