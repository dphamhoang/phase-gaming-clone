class AppBanner {
  int bannerId;
  String link;

  AppBanner({required this.bannerId, required this.link});
  factory AppBanner.fromJson(Map<String, dynamic> json) =>
      AppBanner(bannerId: json['bannerId'], link: json['link']);
}
