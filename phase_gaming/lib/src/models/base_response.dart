class BaseResponse<T> {
  int statusCode;
  String status;
  String message;
  T data;
  BaseResponse(
      {required this.statusCode,
      required this.status,
      required this.message,
      required this.data});
  factory BaseResponse.fromJson(Map<String, dynamic> json) => BaseResponse(
      statusCode: json['statusCode'],
      status: json['status'],
      message: json['message'],
      data: json['data']);
}
