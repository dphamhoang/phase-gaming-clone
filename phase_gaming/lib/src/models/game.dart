import 'package:json_annotation/json_annotation.dart';
import 'package:phase_gaming/src/models/media_image.dart';
import 'package:phase_gaming/src/models/media_video.dart';
import 'package:phase_gaming/src/models/picture.dart';
import 'package:phase_gaming/src/models/platform.dart';
import 'package:phase_gaming/src/models/screenshot.dart';
import 'package:phase_gaming/src/models/tag.dart';

part 'model_generator/game.g.dart';

@JsonSerializable(includeIfNull: true)
class Game {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'desc')
  String desc;
  @JsonKey(name: 'releaseDate')
  String? releaseDate;
  @JsonKey(name: 'rating')
  double? rating;
  @JsonKey(name: 'totalPlaytime')
  int? totalPlaytime;
  @JsonKey(name: 'nowPlaying')
  int? nowPlaying;
  @JsonKey(name: 'rentingPrice')
  int? rentingPrice;
  @JsonKey(name: 'creditRequired')
  int? creditRequired;
  @JsonKey(name: 'payTypeId')
  int? payTypeId;
  @JsonKey(name: 'payTypeName')
  String payTypeName;
  @JsonKey(name: 'tile')
  String tile;
  @JsonKey(name: 'pegiAge')
  int? pegiAge;
  @JsonKey(name: 'banner')
  MediaImage banner;
  @JsonKey(name: 'background')
  MediaImage? background;
  @JsonKey(name: 'platforms')
  List<Platform> platforms;
  @JsonKey(name: 'tags')
  List<Tag> tags;
  @JsonKey(name: 'pictures')
  List<Picture>? pictures;
  @JsonKey(name: 'screenshots')
  List<Screenshot>? screenshots;
  @JsonKey(name: 'trailers')
  List<MediaVideo>? trailers;
  @JsonKey(name: 'gamePlayClips')
  List<MediaVideo>? gamePlayClips;

  Game(
      {required this.id,
      required this.name,
      required this.desc,
      required this.releaseDate,
      required this.rating,
      required this.totalPlaytime,
      required this.nowPlaying,
      required this.rentingPrice,
      required this.creditRequired,
      required this.platforms,
      required this.payTypeId,
      required this.payTypeName,
      required this.banner,
      required this.tags,
      required this.tile,
      required this.pictures,
      required this.screenshots,
      required this.trailers,
      required this.background,
      required this.gamePlayClips});
  factory Game.fromJson(Map<String, dynamic> json) => _$GameFromJson(json);
  Map<String, dynamic> toJson() => _$GameToJson(this);
}
