import 'package:json_annotation/json_annotation.dart';

part 'model_generator/media_image.g.dart';

@JsonSerializable()
class MediaImage {
  @JsonKey(name: "_200x112")
  String x200x112;
  @JsonKey(name: '_420x236')
  String x420x236;
  @JsonKey(name: '_640x360')
  String x640x360;
  @JsonKey(name: '_1280x720')
  String x1280x720;
  @JsonKey(name: '_1920x1080')
  String x1920x1080;
  @JsonKey(name: '_2560x1440')
  String x2560x1440;
  MediaImage(
      {required this.x200x112,
      required this.x420x236,
      required this.x640x360,
      required this.x1280x720,
      required this.x1920x1080,
      required this.x2560x1440});
  factory MediaImage.fromJson(Map<String, dynamic> json) =>
      _$MediaImageFromJson(json);
  Map<String, dynamic> toJson() => _$MediaImageToJson(this);
}
