import 'package:json_annotation/json_annotation.dart';

part 'model_generator/media_video.g.dart';

@JsonSerializable(includeIfNull: true)
class MediaVideo {
  @JsonKey(name: 'link')
  String? link;
  @JsonKey(name: '_320x180')
  String? x320x180;
  @JsonKey(name: '_640x360')
  String? x640x360;
  @JsonKey(name: '_854x480')
  String? x854x480;
  @JsonKey(name: '_1920x1080')
  String? x1920x1080;

  MediaVideo(
      {required this.x320x180,
      required this.x640x360,
      required this.x854x480,
      required this.link,
      required this.x1920x1080});
  factory MediaVideo.fromJson(Map<String, dynamic> json) =>
      _$MediaVideoFromJson(json);
  Map<String, dynamic> toJson() => _$MediaVideoToJson(this);
}
