// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../game.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Game _$GameFromJson(Map<String, dynamic> json) => Game(
      id: json['id'] as int,
      name: json['name'] as String,
      desc: json['desc'] as String,
      releaseDate: json['releaseDate'] as String?,
      rating: (json['rating'] as num?)?.toDouble(),
      totalPlaytime: json['totalPlaytime'] as int?,
      nowPlaying: json['nowPlaying'] as int?,
      rentingPrice: json['rentingPrice'] as int?,
      creditRequired: json['creditRequired'] as int?,
      platforms: (json['platforms'] as List<dynamic>)
          .map((e) => Platform.fromJson(e as Map<String, dynamic>))
          .toList(),
      payTypeId: json['payTypeId'] as int?,
      payTypeName: json['payTypeName'] as String,
      banner: MediaImage.fromJson(json['banner'] as Map<String, dynamic>),
      tags: (json['tags'] as List<dynamic>)
          .map((e) => Tag.fromJson(e as Map<String, dynamic>))
          .toList(),
      tile: json['tile'] as String,
      pictures: (json['pictures'] as List<dynamic>?)
          ?.map((e) => Picture.fromJson(e as Map<String, dynamic>))
          .toList(),
      screenshots: (json['screenshots'] as List<dynamic>?)
          ?.map((e) => Screenshot.fromJson(e as Map<String, dynamic>))
          .toList(),
      trailers: (json['trailers'] as List<dynamic>?)
          ?.map((e) => MediaVideo.fromJson(e as Map<String, dynamic>))
          .toList(),
      background: json['background'] == null
          ? null
          : MediaImage.fromJson(json['background'] as Map<String, dynamic>),
      gamePlayClips: (json['gamePlayClips'] as List<dynamic>?)
          ?.map((e) => MediaVideo.fromJson(e as Map<String, dynamic>))
          .toList(),
    )..pegiAge = json['pegiAge'] as int?;

Map<String, dynamic> _$GameToJson(Game instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'desc': instance.desc,
      'releaseDate': instance.releaseDate,
      'rating': instance.rating,
      'totalPlaytime': instance.totalPlaytime,
      'nowPlaying': instance.nowPlaying,
      'rentingPrice': instance.rentingPrice,
      'creditRequired': instance.creditRequired,
      'payTypeId': instance.payTypeId,
      'payTypeName': instance.payTypeName,
      'tile': instance.tile,
      'pegiAge': instance.pegiAge,
      'banner': instance.banner,
      'background': instance.background,
      'platforms': instance.platforms,
      'tags': instance.tags,
      'pictures': instance.pictures,
      'screenshots': instance.screenshots,
      'trailers': instance.trailers,
      'gamePlayClips': instance.gamePlayClips,
    };
