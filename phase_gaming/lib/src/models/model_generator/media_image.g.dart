// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../media_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MediaImage _$MediaImageFromJson(Map<String, dynamic> json) => MediaImage(
      x200x112: json['_200x112'] as String,
      x420x236: json['_420x236'] as String,
      x640x360: json['_640x360'] as String,
      x1280x720: json['_1280x720'] as String,
      x1920x1080: json['_1920x1080'] as String,
      x2560x1440: json['_2560x1440'] as String,
    );

Map<String, dynamic> _$MediaImageToJson(MediaImage instance) =>
    <String, dynamic>{
      '_200x112': instance.x200x112,
      '_420x236': instance.x420x236,
      '_640x360': instance.x640x360,
      '_1280x720': instance.x1280x720,
      '_1920x1080': instance.x1920x1080,
      '_2560x1440': instance.x2560x1440,
    };
