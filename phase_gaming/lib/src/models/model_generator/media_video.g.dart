// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../media_video.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MediaVideo _$MediaVideoFromJson(Map<String, dynamic> json) => MediaVideo(
      x320x180: json['_320x180'] as String?,
      x640x360: json['_640x360'] as String?,
      x854x480: json['_854x480'] as String?,
      link: json['link'] as String?,
      x1920x1080: json['_1920x1080'] as String?,
    );

Map<String, dynamic> _$MediaVideoToJson(MediaVideo instance) =>
    <String, dynamic>{
      'link': instance.link,
      '_320x180': instance.x320x180,
      '_640x360': instance.x640x360,
      '_854x480': instance.x854x480,
      '_1920x1080': instance.x1920x1080,
    };
