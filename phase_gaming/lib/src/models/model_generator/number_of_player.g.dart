// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../number_of_player.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NumberOfPlayer _$NumberOfPlayerFromJson(Map<String, dynamic> json) =>
    NumberOfPlayer(
      numberOfPlayerId: json['numberOfPlayerId'] as int,
      nop: json['nop'] as String,
    );

Map<String, dynamic> _$NumberOfPlayerToJson(NumberOfPlayer instance) =>
    <String, dynamic>{
      'numberOfPlayerId': instance.numberOfPlayerId,
      'nop': instance.nop,
    };
