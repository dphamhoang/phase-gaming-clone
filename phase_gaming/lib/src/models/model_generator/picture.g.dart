// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../picture.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Picture _$PictureFromJson(Map<String, dynamic> json) => Picture(
      json['_200x112'] as String,
      json['_420x236'] as String,
      json['_640x360'] as String,
      json['_1280x720'] as String,
      json['_1920x1080'] as String,
      json['_2560x1440'] as String,
      pictureId: json['pictureId'] as int,
    );

Map<String, dynamic> _$PictureToJson(Picture instance) => <String, dynamic>{
      '_200x112': instance.x200x112,
      '_420x236': instance.x420x236,
      '_640x360': instance.x640x360,
      '_1280x720': instance.x1280x720,
      '_1920x1080': instance.x1920x1080,
      '_2560x1440': instance.x2560x1440,
      'pictureId': instance.pictureId,
    };
