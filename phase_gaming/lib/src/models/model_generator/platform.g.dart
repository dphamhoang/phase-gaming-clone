// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../platform.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Platform _$PlatformFromJson(Map<String, dynamic> json) => Platform(
      platformId: json['platformId'] as int,
      platformName: json['platformName'] as String,
      imageLoginPlatform: json['imageLoginPlatform'] as String?,
      imageRememberPlatform: json['imageRememberPlatform'] as String?,
      gamePlatformId: json['gamePlatformId'] as String?,
    );

Map<String, dynamic> _$PlatformToJson(Platform instance) => <String, dynamic>{
      'platformId': instance.platformId,
      'platformName': instance.platformName,
      'imageLoginPlatform': instance.imageLoginPlatform,
      'imageRememberPlatform': instance.imageRememberPlatform,
      'gamePlatformId': instance.gamePlatformId,
    };
