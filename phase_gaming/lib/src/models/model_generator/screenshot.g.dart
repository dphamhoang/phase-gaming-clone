// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../screenshot.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Screenshot _$ScreenshotFromJson(Map<String, dynamic> json) => Screenshot(
      json['_200x112'] as String,
      json['_420x236'] as String,
      json['_640x360'] as String,
      json['_1280x720'] as String,
      json['_1920x1080'] as String,
      json['_2560x1440'] as String,
      id: json['id'] as int,
    );

Map<String, dynamic> _$ScreenshotToJson(Screenshot instance) =>
    <String, dynamic>{
      '_200x112': instance.x200x112,
      '_420x236': instance.x420x236,
      '_640x360': instance.x640x360,
      '_1280x720': instance.x1280x720,
      '_1920x1080': instance.x1920x1080,
      '_2560x1440': instance.x2560x1440,
      'id': instance.id,
    };
