// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../support_play_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SupportPlayType _$SupportPlayTypeFromJson(Map<String, dynamic> json) =>
    SupportPlayType(
      id: json['id'] as int,
      type: json['type'] as String,
    );

Map<String, dynamic> _$SupportPlayTypeToJson(SupportPlayType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
    };
