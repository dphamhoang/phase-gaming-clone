import 'package:json_annotation/json_annotation.dart';

part 'model_generator/number_of_player.g.dart';

@JsonSerializable()
class NumberOfPlayer {
  @JsonKey(name: 'numberOfPlayerId')
  int numberOfPlayerId;
  @JsonKey(name: 'nop')
  String nop;
  NumberOfPlayer({required this.numberOfPlayerId, required this.nop});
  factory NumberOfPlayer.fromJson(Map<String, dynamic> json) =>
      _$NumberOfPlayerFromJson(json);
  Map<String, dynamic> toJson() => _$NumberOfPlayerToJson(this);
}
