import 'package:json_annotation/json_annotation.dart';

part 'model_generator/platform.g.dart';

@JsonSerializable()
class Platform {
  @JsonKey(name: 'platformId')
  int platformId;
  @JsonKey(name: 'platformName')
  String platformName;
  @JsonKey(name: 'imageLoginPlatform')
  String? imageLoginPlatform;
  @JsonKey(name: 'imageRememberPlatform')
  String? imageRememberPlatform;
  @JsonKey(name: 'gamePlatformId')
  String? gamePlatformId;

  Platform(
      {required this.platformId,
      required this.platformName,
      this.imageLoginPlatform,
      this.imageRememberPlatform,
      required this.gamePlatformId});

  factory Platform.fromJson(Map<String, dynamic> json) =>
      _$PlatformFromJson(json);
  Map<String, dynamic> toJson() => _$PlatformToJson(this);
}
