import 'package:json_annotation/json_annotation.dart';
import 'package:phase_gaming/src/models/media_image.dart';

part 'model_generator/screenshot.g.dart';

@JsonSerializable()
class Screenshot extends MediaImage {
  @JsonKey(name: 'id')
  int id;
  Screenshot(String x200x112, String x420x236, String x640x360,
      String x1280x720, String x1920x1080, String x2560x1440,
      {required this.id})
      : super(
            x200x112: x200x112,
            x420x236: x420x236,
            x640x360: x640x360,
            x1280x720: x1280x720,
            x1920x1080: x1920x1080,
            x2560x1440: x2560x1440);

  factory Screenshot.fromJson(Map<String, dynamic> json) =>
      _$ScreenshotFromJson(json);
  Map<String, dynamic> toJson() => _$ScreenshotToJson(this);
}
