import 'package:json_annotation/json_annotation.dart';

part 'model_generator/support_play_type.g.dart';

@JsonSerializable()
class SupportPlayType {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'type')
  String type;
  SupportPlayType({required this.id, required this.type});
  factory SupportPlayType.fromJson(Map<String, dynamic> json) =>
      _$SupportPlayTypeFromJson(json);
  Map<String, dynamic> toJson() => _$SupportPlayTypeToJson(this);
}
