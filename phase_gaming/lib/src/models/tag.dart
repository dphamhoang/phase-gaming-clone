import 'package:json_annotation/json_annotation.dart';

part 'model_generator/tag.g.dart';

@JsonSerializable()
class Tag {
  @JsonKey(name: 'tagId')
  int tagId;
  @JsonKey(name: 'tagName')
  String tagName;
  Tag({required this.tagId, required this.tagName});
  factory Tag.fromJson(Map<String, dynamic> json) => _$TagFromJson(json);
  Map<String, dynamic> toJson() => _$TagToJson(this);
}
