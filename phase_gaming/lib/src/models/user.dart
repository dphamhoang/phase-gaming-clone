import 'package:json_annotation/json_annotation.dart';

part 'model_generator/user.g.dart';

@JsonSerializable(includeIfNull: true)
class User {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'userName')
  String userName;
  @JsonKey(name: 'fullName')
  String fullName;
  @JsonKey(name: 'displayName')
  String displayName;
  @JsonKey(name: 'email')
  String email;
  @JsonKey(name: 'firstName')
  String? firstName;
  @JsonKey(name: 'lastName')
  String? lastName;
  @JsonKey(name: 'phoneNumber')
  String? phoneNumber;
  User({
    required this.id,
    required this.userName,
    required this.fullName,
    required this.displayName,
    required this.email,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
