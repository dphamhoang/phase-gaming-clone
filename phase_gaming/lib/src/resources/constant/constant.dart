class Constant {
  static String logIn = 'Login';
  static String signUp = 'Sign Up';
  static String baseHttpsUrl = 'https://roragame.rorasoft.com/api/';
  static String prefInitializeAppKey = 'initializeApp';
  static String prefIsLoggedInKey = 'isLoggedIn';
  static String newGame = 'New Game';
  static String trendingGame = 'Trending Game';
  static String hotGame = "What\'s Hot";
  static String editorChoice = "Editor\'s choice";
  static int pageSize = 20;
}
