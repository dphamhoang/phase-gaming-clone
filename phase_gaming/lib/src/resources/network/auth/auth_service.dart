import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/models/api/login_response.dart';
import 'package:phase_gaming/src/models/user.dart';
import 'package:phase_gaming/src/resources/constant/constant.dart';
import 'package:phase_gaming/src/resources/network/header_interceptor.dart';
import 'package:phase_gaming/src/utils/converter/json_handler.dart';

part 'auth_service.chopper.dart';

@ChopperApi()
abstract class AuthService extends ChopperService {
  @Post(path: 'auth/login')
  Future<Response<LoginResponse>> login(@Body() Map<String, dynamic> body);

  @Get(path: 'users/info')
  Future<Response<User>> getUserInfo();

  static AuthService create() {
    final client = ChopperClient(
        baseUrl: Constant.baseHttpsUrl,
        interceptors: [HeaderInterceptor(), HttpLoggingInterceptor()],
        converter: JsonHandler(
            {LoginResponse: (data) => LoginResponse.fromJson(data)}),
        services: [_$AuthService()]);
    return _$AuthService(client);
  }
}
