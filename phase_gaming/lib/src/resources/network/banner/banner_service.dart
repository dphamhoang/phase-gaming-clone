import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/models/banner.dart';
import 'package:phase_gaming/src/resources/constant/constant.dart';
import 'package:phase_gaming/src/utils/converter/json_handler.dart';

part 'banner_service.chopper.dart';

@ChopperApi()
abstract class BannerService extends ChopperService {
  @Get(path: 'banner')
  Future<Response<List<AppBanner>>> getBanners();

  static BannerService create() {
    final client = ChopperClient(
        baseUrl: Constant.baseHttpsUrl,
        converter: JsonHandler({AppBanner: (data) => AppBanner.fromJson(data)}),
        errorConverter: JsonConverter(),
        services: [_$BannerService()]);
    return _$BannerService(client);
  }
}
