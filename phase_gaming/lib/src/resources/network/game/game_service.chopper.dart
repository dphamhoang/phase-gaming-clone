// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$GameService extends GameService {
  _$GameService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = GameService;

  @override
  Future<Response<List<Game>>> getGames(int page, int pageSize) {
    final $url = 'games/';
    final $params = <String, dynamic>{'page': page, 'page_size': pageSize};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Game>, Game>($request);
  }

  @override
  Future<Response<List<Game>>> getNew(int page, int pageSize) {
    final $url = 'games/new';
    final $params = <String, dynamic>{'page': page, 'page_size': pageSize};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Game>, Game>($request);
  }

  @override
  Future<Response<List<Game>>> getTrending(int page, int pageSize) {
    final $url = 'games/trending';
    final $params = <String, dynamic>{'page': page, 'page_size': pageSize};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Game>, Game>($request);
  }

  @override
  Future<Response<List<Game>>> getHot(int page, int pageSize) {
    final $url = 'games/hot';
    final $params = <String, dynamic>{'page': page, 'page_size': pageSize};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Game>, Game>($request);
  }

  @override
  Future<Response<List<Game>>> getEditorChoice(int page, int pageSize) {
    final $url = 'games/editor_choice';
    final $params = <String, dynamic>{'page': page, 'page_size': pageSize};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Game>, Game>($request);
  }

  @override
  Future<Response<Game>> getGame(int id) {
    final $url = 'games/${id}';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<Game, Game>($request);
  }
}
