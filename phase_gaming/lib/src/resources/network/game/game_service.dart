import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/resources/constant/constant.dart';
import 'package:phase_gaming/src/utils/converter/json_handler.dart';

part 'game_service.chopper.dart';

@ChopperApi(baseUrl: 'games/')
abstract class GameService extends ChopperService {
  @Get()
  Future<Response<List<Game>>> getGames(
      @Query('page') int page, @Query('page_size') int pageSize);

  @Get(path: 'new')
  Future<Response<List<Game>>> getNew(
      @Query("page") int page, @Query('page_size') int pageSize);

  @Get(path: 'trending')
  Future<Response<List<Game>>> getTrending(
      @Query('page') int page, @Query('page_size') int pageSize);

  @Get(path: 'hot')
  Future<Response<List<Game>>> getHot(
      @Query('page') int page, @Query('page_size') int pageSize);

  @Get(path: 'editor_choice')
  Future<Response<List<Game>>> getEditorChoice(
      @Query('page') int page, @Query('page_size') int pageSize);

  @Get(path: '{id}')
  Future<Response<Game>> getGame(@Path('id') int id);

  static GameService create() {
    final client = ChopperClient(
        baseUrl: Constant.baseHttpsUrl,
        converter: JsonHandler({Game: (data) => Game.fromJson(data)}),
        services: [_$GameService()]);
    return _$GameService(client);
  }
}
