import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class HeaderInterceptor implements RequestInterceptor {
  static const String AUTH_HEADER = 'Authorization';
  static const String BEARER = ' Bearer ';

  final _storage = FlutterSecureStorage();

  @override
  FutureOr<Request> onRequest(Request request) async {
    final token = await _storage.read(key: 'token');
    if (request.method != HttpMethod.Post && token != null) {
      Request newRequest =
          request.copyWith(headers: {AUTH_HEADER: BEARER + token});
      return newRequest;
    }
    return request;
  }
}
