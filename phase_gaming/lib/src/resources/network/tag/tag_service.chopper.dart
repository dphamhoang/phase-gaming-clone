// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tag_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$TagService extends TagService {
  _$TagService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = TagService;

  @override
  Future<Response<List<Tag>>> getTags() {
    final $url = 'tags';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<Tag>, Tag>($request);
  }
}
