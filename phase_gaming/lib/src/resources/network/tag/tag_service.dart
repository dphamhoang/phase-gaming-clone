import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/models/tag.dart';
import 'package:phase_gaming/src/resources/constant/constant.dart';
import 'package:phase_gaming/src/utils/converter/json_handler.dart';

part 'tag_service.chopper.dart';

@ChopperApi(baseUrl: 'tags')
abstract class TagService extends ChopperService {
  @Get()
  Future<Response<List<Tag>>> getTags();
  static TagService create() {
    final client = ChopperClient(
      baseUrl: Constant.baseHttpsUrl,
      converter: JsonHandler({Tag: (json) => Tag.fromJson(json)}),
      services: [_$TagService()],
    );
    return _$TagService(client);
  }
}
