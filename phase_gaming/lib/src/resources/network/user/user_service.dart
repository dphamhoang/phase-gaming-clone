import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/models/user.dart';
import 'package:phase_gaming/src/resources/constant/constant.dart';
import 'package:phase_gaming/src/resources/network/header_interceptor.dart';
import 'package:phase_gaming/src/utils/converter/json_handler.dart';

part 'user_service.chopper.dart';

@ChopperApi(baseUrl: 'users/')
abstract class UserService extends ChopperService {
  @Get(path: 'info')
  Future<Response<User>> getUserInfo();

  static UserService create() {
    final client = ChopperClient(
        baseUrl: Constant.baseHttpsUrl,
        interceptors: [HeaderInterceptor(), HttpLoggingInterceptor()],
        converter: JsonHandler({User: (json) => User.fromJson(json)}),
        services: [_$UserService()]);
    return _$UserService(client);
  }
}
