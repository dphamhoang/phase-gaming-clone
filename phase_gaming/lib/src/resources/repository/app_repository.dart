import 'dart:async';

class AppRepository {
  bool initalized;
  AppRepository({required this.initalized});

  void initializeApp() {
    Timer(const Duration(seconds: 2), () {
      this.initalized = true;
    });
  }
}
