import 'package:chopper/chopper.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:phase_gaming/src/models/api/login_response.dart';
import 'package:phase_gaming/src/models/user.dart';
import 'package:phase_gaming/src/resources/network/auth/auth_service.dart';
import 'package:phase_gaming/src/resources/network/user/user_service.dart';

class AuthRepository {
  final _storage = FlutterSecureStorage();
  final AuthService authService = AuthService.create();
  final UserService userService = UserService.create();

  Future<Response<LoginResponse>> login(String email, String password) async {
    final result =
        await authService.login({'username': email, 'password': password});
    if (result.statusCode == 200) {
      await _storage.write(key: 'token', value: result.body!.token.toString());
      isLoggedIn();
    }
    return result;
  }

  Future<Response<User>> getUserInfo() async {
    final result = await userService.getUserInfo();
    if (result.statusCode == 401 && await _storage.read(key: 'token') != null) {
      await _storage.delete(key: 'token');
    }
    return result;
  }

  Future<bool> isLoggedIn() async {
    final getUserInfoResult = await getUserInfo();
    if (await _storage.read(key: 'token') != null ||
        getUserInfoResult.body != null) {
      return true;
    }
    return false;
  }

  Future<bool> logOut() async {
    await _storage.delete(key: 'token');
    return true;
  }
}
