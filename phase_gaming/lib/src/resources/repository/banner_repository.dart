import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/models/banner.dart';
import 'package:phase_gaming/src/resources/network/banner/banner_service.dart';

class BannerRepository {
  final BannerService bannerService = BannerService.create();

  Future<Response<List<AppBanner>>> getBanner() async {
    final banner = await bannerService.getBanners();
    return banner;
  }
}
