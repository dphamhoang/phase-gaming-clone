import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/models/tag.dart';
import 'package:phase_gaming/src/resources/network/game/game_service.dart';
import 'package:phase_gaming/src/resources/network/tag/tag_service.dart';

class GameRepository {
  final GameService gameService = GameService.create();
  final TagService tagService = TagService.create();

  Future<Response<List<Game>>> getNewGame(int page, int pageSize) async {
    final result = await gameService.getNew(page, pageSize);
    return result;
  }

  Future<Response<List<Game>>> getTrendingGame(int page, int pageSize) async {
    final trendingGames = await gameService.getTrending(page, pageSize);
    return trendingGames;
  }

  Future<Response<List<Game>>> getHotGame(int page, int pageSize) async {
    final hotGames = await gameService.getHot(page, pageSize);
    return hotGames;
  }

  Future<Response<List<Game>>> getEditorChoice(int page, int pageSize) async {
    final editorChoice = await gameService.getEditorChoice(page, pageSize);
    return editorChoice;
  }

  Future<Response<List<Tag>>> getTags() async {
    final tags = await tagService.getTags();
    return tags;
  }

  Future<Response<Game>> getGame(int id) async {
    final game = await gameService.getGame(id);
    return game;
  }
}
