import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/assets/values/list_style.dart';
import 'package:phase_gaming/src/models/game.dart';

class ListGame extends StatelessWidget {
  final List<Game> games;
  final ItemStyle itemStyle;
  final ItemSize? itemSize;
  final ListDirection listDirection;

  ListGame(
      {required this.games, required this.itemStyle, required this.listDirection, this.itemSize});

  Widget _buildListItem(Game game) {
    return Container();
  }

  Widget _buildListGame() {
    return ListView.separated(
      itemCount: games.length,
      separatorBuilder: (context, index) {
        return SizedBox(
          width: Dimens.MAX_SPACE,
        );
      },
      itemBuilder: (context, index) {
        return _buildListItem(games[index]);
      },
    );
  }

  Widget _buildBody() {
    return Container(
      child: _buildListGame(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildBody(),
    );
  }
}
