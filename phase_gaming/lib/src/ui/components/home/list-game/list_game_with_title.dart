import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/assets/values/list_style.dart';
import 'package:phase_gaming/src/blocs/app/app_bloc.dart';
import 'package:phase_gaming/src/blocs/app/app_event.dart';
import 'package:phase_gaming/src/blocs/game/game_bloc.dart';
import 'package:phase_gaming/src/blocs/game/game_state.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/components/item/item_landscape.dart';
import 'package:phase_gaming/src/ui/components/item/item_portrait.dart';
import 'package:phase_gaming/src/ui/components/item/item_vertical.dart';
import 'package:phase_gaming/src/ui/components/shared/title_component.dart';

class ListGameWithTitle extends StatelessWidget {
  final List<Game> games;
  final String title;
  final ItemStyle itemStyle;
  final ListDirection listDirection;
  final ItemSize? itemSize;

  ListGameWithTitle(
      {required this.games,
      required this.title,
      required this.itemStyle,
      required this.listDirection,
      this.itemSize});

  Widget _buildListItem(Game game) {
    if (itemStyle == ItemStyle.portrait) {
      return ItemPortrait(
        game: game,
        itemStyle: itemStyle,
        itemSize: itemSize,
      );
    } else if (itemStyle == ItemStyle.landscape) {
      return ItemLandscape(game: game);
    }
    return ItemVertical(game: game);
  }

  Widget _buildListGame(BuildContext context) {
    return Container(
      height: itemStyle == ItemStyle.landscape
          ? MediaQuery.of(context).size.height / 5.5
          : MediaQuery.of(context).size.height / 2.2,
      child: ListView.separated(
        itemCount: games.length,
        scrollDirection: listDirection == ListDirection.vertical
            ? Axis.vertical
            : Axis.horizontal,
        separatorBuilder: (context, index) {
          return SizedBox(
            width: Dimens.MAX_SPACE,
          );
        },
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              BlocProvider.of<AppBloc>(context)
                  .add(GameDetailEvent(id: games[index].id));
            },
            child: _buildListItem(games[index]),
          );
        },
      ),
    );
  }

  Widget _buildMoreButton() {
    return Container(
      width: 30,
      height: 30,
      margin: EdgeInsets.only(right: Dimens.MEDIUM_SPACE + 5),
      decoration: BoxDecoration(
          color: AppColors.colorPrimary,
          borderRadius: BorderRadius.all(const Radius.circular(100))),
      child: IconButton(
        icon: Icon(Icons.more_horiz),
        color: Colors.black,
        iconSize: 24,
        padding: EdgeInsets.all(2),
        onPressed: () {},
      ),
    );
  }

  Widget _buildTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [TitleComponent(title: title), _buildMoreButton()],
    );
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      child: Column(
        children: [_buildTitle(), _buildListGame(context)],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => GameBloc(GameState()),
      child: _buildBody(context),
    );
  }
}
