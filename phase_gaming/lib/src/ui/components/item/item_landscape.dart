import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/components/shared/card_platform.dart';
import 'package:phase_gaming/src/ui/components/shared/pay_type.dart';

class ItemLandscape extends StatelessWidget {
  final Game game;
  ItemLandscape({required this.game});

  Widget _buildPlatform() {
    return Positioned(
      child: CardPlatform(
        platforms: game.platforms,
        width: 16,
        height: 16,
      ),
      top: 4,
      right: 3,
    );
  }

  Widget _buildPayType() {
    return Positioned(
        bottom: 0,
        height: 40,
        child: PayType(
          width: 215,
          payTypeName: game.payTypeName,
        ));
  }

  Widget _buildImage(BuildContext context) {
    return Container(
      width: 215,
      height: MediaQuery.of(context).size.height / 8,
      decoration: BoxDecoration(
          borderRadius:
              BorderRadius.all(const Radius.circular(Dimens.MAX_RADIUS)),
          image: DecorationImage(
              image: NetworkImage(game.banner.x2560x1440), fit: BoxFit.cover)),
      child: Stack(
        children: [_buildPlatform(), _buildPayType()],
      ),
    );
  }

  Widget _buildGameName(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Dimens.MIN_SPACE),
      child: Text(
        game.name,
        style: Theme.of(context).textTheme.headline3,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          EdgeInsets.only(top: Dimens.MAX_SPACE, left: Dimens.MIN_SPACE * 3),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [_buildImage(context), _buildGameName(context)],
      ),
    );
  }
}
