import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/assets/values/list_style.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/components/shared/card_platform.dart';
import 'package:phase_gaming/src/ui/components/shared/pay_type.dart';

class ItemPortrait extends StatelessWidget {
  final Game game;
  final ItemStyle itemStyle;
  final ItemSize? itemSize;
  final Function()? onPress;
  ItemPortrait({required this.game, required this.itemStyle, this.onPress, this.itemSize});

  Widget _buildPayType(BuildContext context) {
    return Positioned(
        bottom: 0,
        height: 40,
        child: PayType(
          width: itemSize == ItemSize.portrait ? 220 : 181,
          payTypeName: game.payTypeName,
        ));
  }

  Widget _buildPlatform() {
    return Positioned(
      child: CardPlatform(
        platforms: game.platforms,
        width: 16,
        height: 16,
      ),
      top: 4,
      right: 3,
    );
  }

  Widget _buildImage(BuildContext context) {
    return Hero(
        tag: 'card-image',
        child: Container(
          width: itemSize == ItemSize.portrait ? 220 : 181,
          height: MediaQuery.of(context).size.height / 2.8,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(Dimens.MEDIUM_RADIUS)),
            image: DecorationImage(
              image: NetworkImage(game.tile),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [_buildPayType(context), _buildPlatform()],
          ),
        ));
  }

  Widget _buildGameName(BuildContext context) {
    return Container(
      width: itemSize == ItemSize.smallPortrait ? 181 : null,
      margin: EdgeInsets.only(top: 10),
      child: Text(
        game.name,
        maxLines: itemSize == ItemSize.smallPortrait ? 2 : null,
        style: Theme.of(context).textTheme.headline3,
        overflow: TextOverflow.clip,
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        height: MediaQuery.of(context).size.height / 2,
        margin: EdgeInsets.only(top: Dimens.MAX_SPACE, left: Dimens.MIN_SPACE * 3),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [_buildImage(context), _buildGameName(context)],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _buildBody(context);
  }
}
