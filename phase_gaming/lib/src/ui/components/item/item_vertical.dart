import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/components/shared/card_platform.dart';
import 'package:phase_gaming/src/ui/components/shared/pay_type.dart';
import 'package:phase_gaming/src/utils/converter/string_format.dart';

class ItemVertical extends StatelessWidget {
  final Game game;
  ItemVertical({required this.game});

  Widget _buildPlatform() {
    return Positioned(
      child: CardPlatform(
        platforms: game.platforms,
        width: 10,
        height: 10,
      ),
      top: 4,
      right: 3,
    );
  }

  Widget _buildPayType(BuildContext context) {
    return Positioned(
        bottom: 0,
        height: 40,
        child: PayType(
          width: MediaQuery.of(context).size.width / 2.75,
          payTypeName: game.payTypeName,
        ));
  }

  Widget _buildImage(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2.75,
      height: 85,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            const Radius.circular(Dimens.MAX_RADIUS),
          ),
          image: DecorationImage(
              image: NetworkImage(game.banner.x2560x1440),
              fit: BoxFit.fitWidth)),
      child: Stack(
        children: [_buildPayType(context), _buildPlatform()],
      ),
    );
  }

  Widget _buildInfo(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            game.name,
            style: Theme.of(context).textTheme.headline3,
          ),
          Text(
            StringFormat.parseHtml(game.desc),
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          ),
          Text(
            StringFormat.itemVerticalTagFormat(game.tags),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          )
        ],
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      child: Row(
        children: [
          _buildImage(context),
          SizedBox(
            width: Dimens.MIN_SPACE + 4,
          ),
          _buildInfo(context)
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          left: Dimens.NORMAL_SPACE - 4, bottom: Dimens.NORMAL_SPACE - 5),
      child: _buildBody(context),
    );
  }
}
