import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/platform.dart';

class CardPlatform extends StatelessWidget {
  final List<Platform> platforms;
  double width, height;
  CardPlatform(
      {required this.platforms, required this.width, required this.height});
  List<Widget> _buildPlatform() {
    List<Widget> platformList = <Widget>[];
    platforms.forEach((element) {
      switch (element.platformName) {
        case 'Steam':
          platformList.add(Container(
            child: Image(
              image: AssetImage('assets/platform/steam.png'),
              width: width,
              height: height,
            ),
          ));
          break;
        case "Battle.net":
          platformList.add(Container(
            margin: EdgeInsets.symmetric(horizontal: Dimens.MIN_SPACE),
            child: SvgPicture.asset(
              'assets/platform/battle-net.svg',
              width: width,
              height: height,
            ),
          ));
          break;
        case 'Epic':
          platformList.add(Container(
            child: Image(
              image: AssetImage('assets/platform/epic.png'),
              width: width,
              height: height,
            ),
          ));
          break;
      }
    });
    return platformList;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
          color: AppColors.colorPrimaryDark,
          borderRadius: BorderRadius.all(const Radius.circular(100))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: _buildPlatform(),
      ),
    );
  }
}
