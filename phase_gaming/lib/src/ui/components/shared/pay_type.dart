import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';

class PayType extends StatelessWidget {
  final double width;
  final String payTypeName;

  PayType({required this.width, required this.payTypeName});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: width,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              tileMode: TileMode.mirror,
              stops: [
                0.1,
                0.5,
                1
              ],
              colors: [
                Colors.black.withOpacity(0.9),
                HexColor('#BF000000'),
                Colors.transparent
              ]),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(Dimens.MEDIUM_RADIUS),
              bottomRight: Radius.circular(Dimens.MEDIUM_RADIUS))),
      child: Container(
        alignment: Alignment.bottomLeft,
        margin: EdgeInsets.only(left: 10, bottom: 10),
        child: Text(
          payTypeName,
          style: Theme.of(context).textTheme.caption,
        ),
      ),
    );
  }
}
