import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';

class TitleComponent extends StatelessWidget {
  final String title;
  TitleComponent({required this.title});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: Dimens.NORMAL_SPACE),
            height: 28,
            width: 7,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(const Radius.circular(10)),
                color: AppColors.colorPrimary),
          ),
          Text(
            title,
            style: Theme.of(context).textTheme.headline2,
          )
        ],
      ),
    );
  }
}
