import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/ui/components/loading.dart';
import 'package:video_player/video_player.dart';

class VideoView extends StatefulWidget {
  final String videoUrl;
  VideoView({Key? key, required this.videoUrl}) : super(key: key);

  @override
  _VideoViewState createState() => _VideoViewState();
}

class _VideoViewState extends State<VideoView> {
  late VideoPlayerController _videoPlayerController;
  @override
  void initState() {
    super.initState();
    _videoPlayerController = VideoPlayerController.network(widget.videoUrl);
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    super.dispose();
  }

  Future<bool> _started() async {
    await _videoPlayerController.initialize();
    _videoPlayerController..setVolume(0);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: FutureBuilder<bool>(
      future: _started(),
      builder: (context, snapshot) {
        if (snapshot.data == true) {
          return Stack(
            children: [
              ClipRRect(
                borderRadius:
                    BorderRadius.all(const Radius.circular(Dimens.MAX_RADIUS)),
                child: AspectRatio(
                  aspectRatio: _videoPlayerController.value.aspectRatio,
                  child: VideoPlayer(_videoPlayerController),
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: FittedBox(
                  child: IconButton(
                    splashColor: Colors.transparent,
                    onPressed: () {
                      setState(() {
                        _videoPlayerController.value.isPlaying
                            ? _videoPlayerController.pause()
                            : _videoPlayerController.play();
                      });
                    },
                    icon: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(const Radius.circular(30))),
                      child: _videoPlayerController.value.isPlaying
                          ? Icon(
                              Icons.pause,
                              size: 10,
                              color: Colors.black,
                            )
                          : Icon(
                              Icons.play_arrow,
                              size: 10,
                              color: Colors.black,
                            ),
                    ),
                  ),
                ),
              ),
            ],
          );
        }
        return Container(
          height: Dimens.GAME_ITEM_HEIGHT,
          width: MediaQuery.of(context).size.width,
          child: Loading(),
        );
      },
    ));
  }
}
