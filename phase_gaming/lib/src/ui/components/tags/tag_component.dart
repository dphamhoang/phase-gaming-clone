import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/tag.dart';

class TagsComponent extends StatelessWidget {
  final List<Tag> tags;
  TagsComponent({required this.tags});

  Widget _buildChoiceChip() {
    return ListView.separated(
        itemBuilder: (context, index) {
          return Container(
              margin: index == 0
                  ? EdgeInsets.only(left: Dimens.MIN_SPACE * 3)
                  : null,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: AppColors.colorPrimary,
                  borderRadius: BorderRadius.all(
                      const Radius.circular(Dimens.MAX_RADIUS))),
              width: 90,
              child: Text(
                tags[index].tagName,
                textAlign: TextAlign.center,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white),
              ));
        },
        separatorBuilder: (context, index) {
          return SizedBox(
            width: Dimens.MIN_SPACE * 2,
          );
        },
        scrollDirection: Axis.horizontal,
        itemCount: tags.length);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Container(
        child: _buildChoiceChip(),
      ),
    );
  }
}
