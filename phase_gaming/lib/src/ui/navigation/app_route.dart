import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:phase_gaming/src/app.dart';
import 'package:phase_gaming/src/blocs/app/app_bloc.dart';
import 'package:phase_gaming/src/blocs/app/app_state.dart';
import 'package:phase_gaming/src/ui/screen/auth/login_screen.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/game_detail.dart';
import 'package:phase_gaming/src/ui/screen/splash/splash_screen.dart';

class AppPath {
  static String homePath = '/';
  static String loginPath = '/login';
  static String splashScreenPath = '/splash';
  static String gameDetailPath = '/game-detail';
}

class AppRoute extends RouterDelegate
    with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  @override
  GlobalKey<NavigatorState> navigatorKey;

  AppRoute() : navigatorKey = GlobalKey<NavigatorState>();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, state) {
      return Navigator(
        key: navigatorKey,
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }
          if (route.settings.name == AppPath.gameDetailPath) {
            state.gameId = 0;
          }
          return true;
        },
        pages: [
          if (!state.initializeApp) SplashScreen.page(),
          if (state.initializeApp && !state.isLoggedIn) LoginScreen.page(),
          if (state.isGuest || state.isLoggedIn) App.page(state.currentTab),
          if (state.gameId != 0) GameDetail.page(state.gameId)
        ],
      );
    });
  }

  @override
  Future<void> setNewRoutePath(configuration) async => null;
}
