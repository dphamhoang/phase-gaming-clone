import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/blocs/app/app_bloc.dart';
import 'package:phase_gaming/src/blocs/app/app_event.dart';
import 'package:phase_gaming/src/blocs/login/login_bloc.dart';
import 'package:phase_gaming/src/blocs/login/login_event.dart';
import 'package:phase_gaming/src/blocs/login/login_state.dart';
import 'package:phase_gaming/src/ui/navigation/app_route.dart';

class LoginScreen extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
        name: AppPath.loginPath,
        key: ValueKey(AppPath.loginPath),
        child: LoginScreen());
  }

  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  Widget _buildHeader() {
    return Column(
      children: [
        SizedBox(
          width: 150,
          height: 150,
          child: SvgPicture.asset('assets/image/logo.svg'),
        ),
        Text(
          'Welcome!',
          style: TextStyle(
              fontSize: Dimens.LOGO_TEXT_SIZE,
              color: Colors.white,
              fontStyle: FontStyle.italic),
        ),
        Text(
          'Login to your account',
          style: TextStyle(
            fontSize: Dimens.MIN_TEXT_SIZE,
            color: Colors.white,
          ),
        )
      ],
    );
  }

  Widget _buildEmailAndPasswordInput(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Dimens.MAXX_SPACE),
      child: Column(
        children: [
          TextField(
            controller: emailController,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    const Radius.circular(Dimens.MAX_RADIUS),
                  ),
                ),
                labelText: 'Email'),
          ),
          SizedBox(
            height: Dimens.MEDIUM_SPACE,
          ),
          TextField(
            controller: passwordController,
            textInputAction: TextInputAction.done,
            onSubmitted: (_) {
              BlocProvider.of<LoginBloc>(context).add(LoginSubmitted(
                  email: emailController.value.text,
                  password: passwordController.value.text));
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                      const Radius.circular(Dimens.MAX_RADIUS))),
              labelText: 'Password',
            ),
            obscureText: true,
          ),
          _buildSignInTrouble()
        ],
      ),
    );
  }

  Widget _buildSignInTrouble() {
    return Container(
      alignment: Alignment.centerRight,
      child: TextButton(
        child: Text(
          'Trouble with sign in?',
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
        onPressed: () {},
      ),
    );
  }

  Widget _buildSignInButton() {
    return Row(
      children: [
        Expanded(
            flex: 3,
            child: BlocBuilder<LoginBloc, LoginState>(
              builder: (context, state) {
                if (state.isSubmitting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return TextButton(
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                  const Radius.circular(Dimens.MAX_RADIUS)))),
                      backgroundColor: MaterialStateProperty.all<HexColor>(
                          HexColor('#2D2E2E'))),
                  child: Container(
                    margin:
                        EdgeInsets.symmetric(vertical: Dimens.MIN_SPACE + 5),
                    child: Text(
                      'Sign in',
                    ),
                  ),
                  onPressed: () {
                    BlocProvider.of<LoginBloc>(context).add(LoginSubmitted(
                        email: emailController.value.text,
                        password: passwordController.value.text));
                  },
                );
              },
            )),
        SizedBox(
          width: Dimens.MEDIUM_SPACE,
        ),
        Expanded(
            child: TextButton(
          style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                          const Radius.circular(Dimens.MAX_RADIUS)))),
              backgroundColor:
                  MaterialStateProperty.all<HexColor>(HexColor('#2D2E2E'))),
          child: Container(
            margin: EdgeInsets.symmetric(vertical: Dimens.MIN_SPACE + 5),
            child: Text('Guest'),
          ),
          onPressed: () {
            BlocProvider.of<AppBloc>(context).add(GuestEvent(isGuest: true));
          },
        ))
      ],
    );
  }

  Widget _buildSignUpButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Don\'t have an account ?',
          style: TextStyle(color: Colors.white),
        ),
        TextButton(
            onPressed: () {},
            child: Text(
              'Sign up here',
              style: TextStyle(fontStyle: FontStyle.italic),
            ))
      ],
    );
  }

  Widget _buildBody(BuildContext context, LoginState state) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: IntrinsicHeight(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(horizontal: Dimens.MAXXXX_SPACE),
            child: Column(
              children: [
                Spacer(),
                _buildHeader(),
                Spacer(flex: 1,),
                _buildEmailAndPasswordInput(context),
                Spacer(flex: 1,),
                _buildSignInButton(),
                Spacer(flex: 3,),
                _buildSignUpButton(),
                SizedBox(height: Dimens.MEDIUM_SPACE,)
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LoginBloc(LoginState()),
      child: BlocConsumer<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state.isSuccess) {
              BlocProvider.of<AppBloc>(context).add(LoggedInEvent());
            }
          },
          builder: (context, state) => _buildBody(context, state)),
    );
  }
}
