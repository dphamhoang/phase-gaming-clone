import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  Widget buildHeader(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      height: 200,
      child: Text(
        'Create\n your account',
        style: Theme.of(context).textTheme.headline1,
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        buildTextField('Email'),
        buildTextField('Password'),
        buildTextField('Confirm password'),
        buildCheckBox(),
        Padding(
          padding: EdgeInsets.only(top: 100),
          child: _buildTextButton('Sign up'),
        )
      ],
    );
  }

  Widget _buildFooter() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Already had an account? '),
          TextButton(
              onPressed: () {},
              child: Text(
                'Sign in',
                style: TextStyle(fontStyle: FontStyle.italic),
              ))
        ],
      ),
    );
  }

  Widget buildTextField(String label) {
    return Column(
      children: [
        TextField(
          decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              labelText: label),
        ),
        SizedBox(
          height: 12,
        )
      ],
    );
  }

  Widget buildCheckBox() {
    return Row(
      children: [
        Checkbox(
          value: false,
          onChanged: (value) {},
          side: BorderSide(color: Colors.blue, width: 2),
          splashRadius: 20,
        ),
        Text('Accept Policy')
      ],
    );
  }

  Widget _buildTextButton(String text) {
    return TextButton(onPressed: () {}, child: Text(text));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: Padding(
                padding: EdgeInsets.all(35),
                child: Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [buildHeader(context), _buildBody()],
                      ),
                    ),
                    _buildFooter()
                  ],
                ))),
          )),
    );
  }
}
