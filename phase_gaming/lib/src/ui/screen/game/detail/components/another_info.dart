import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/game.dart';

class AnotherInfo extends StatelessWidget {
  final Game game;
  const AnotherInfo({Key? key, required this.game}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? releasedDate;
    if (game.releaseDate != null) {
      initializeDateFormatting();
      releasedDate =
          DateFormat.yMMMMd().format(DateTime.parse(game.releaseDate!));
    }
    return Container(
      margin: EdgeInsets.only(top: Dimens.MAXX_SPACE),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                'Release: ',
                style: TextStyle(
                    color: Colors.white, fontSize: Dimens.SUB_TITLE_TEXT_SIZE),
              ),
              Text(releasedDate ?? '--')
            ],
          ),
          Row(
            children: [
              Text('Age rating: ',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: Dimens.SUB_TITLE_TEXT_SIZE)),
              Text('${game.pegiAge ?? 0}+')
            ],
          )
        ],
      ),
    );
  }
}
