import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/components/another_info.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/components/game_desc.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/components/game_detail_banner.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/components/game_detail_video.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/components/game_media.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/components/main_info.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/components/play_and_favorite_button.dart';

import 'game_detail_screenshot.dart';

class Body extends StatelessWidget {
  final Game game;
  Body({Key? key, required this.game}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GameDetailBanner(gameBanner: game.banner.x2560x1440, size: size),
            Container(
              margin: EdgeInsets.only(
                left: Dimens.MEDIUM_SPACE,
                bottom: Dimens.MAXX_SPACE,
              ),
              child: Wrap(
                children: [
                  MainInfo(game: game, size: size),
                  PlayAndFavoriteButton(),
                  GameDesc(gameDesc: game.desc),
                  AnotherInfo(game: game),
                  GameMedia(game: game),
                  GameDetailVideo(game: game),
                  GameDetailScreenshot(screenshots: game.screenshots)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
