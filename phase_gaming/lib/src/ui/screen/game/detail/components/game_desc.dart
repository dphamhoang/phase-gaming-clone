import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/utils/converter/string_format.dart';

class GameDesc extends StatelessWidget {
  final String gameDesc;

  const GameDesc({Key? key, required this.gameDesc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ExpandableText(
      StringFormat.parseHtml(gameDesc),
      expandText: 'Read more',
      collapseText: 'Show less',
      animation: true,
      maxLines: 5,
      style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
      linkColor: AppColors.colorPrimary,
    ));
  }
}
