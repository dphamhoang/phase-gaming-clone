import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/blocs/app/app_bloc.dart';
import 'package:phase_gaming/src/blocs/app/app_event.dart';

class GameDetailBanner extends StatelessWidget {
  final Size size;
  final String gameBanner;

  GameDetailBanner({Key? key, required this.gameBanner, required this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size.height / 3.5,
      width: size.width,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(gameBanner), fit: BoxFit.cover)),
      child: Stack(children: [
        Positioned(
            top: Dimens.MAXXXX_SPACE,
            child: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                BlocProvider.of<AppBloc>(context).add(GameDetailEvent(id: 0));
              },
            )),
        Positioned(
            top: Dimens.MAXXXX_SPACE * 4,
            bottom: 0,
            child: Container(
              width: size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    tileMode: TileMode.mirror,
                    stops: [
                      0.1,
                      0.5,
                      1
                    ],
                    colors: [
                      AppColors.colorPrimaryDark,
                      HexColor('#BF000000'),
                      Colors.transparent
                    ]),
              ),
            ))
      ]),
    );
  }
}
