import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/screenshot.dart';
import 'package:phase_gaming/src/ui/components/shared/title_component.dart';

class GameDetailScreenshot extends StatelessWidget {
  final List<Screenshot>? screenshots;

  const GameDetailScreenshot({Key? key, required this.screenshots})
      : super(key: key);

  Widget _buildScreenshotListView() {
    if (screenshots != null) {
      return Container(
        height: Dimens.GAME_ITEM_HEIGHT,
        child: ListView.separated(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return ClipRRect(
                borderRadius: BorderRadius.all(
                    const Radius.circular(Dimens.MEDIUM_RADIUS)),
                child: Image.network(screenshots![index].x2560x1440),
              );
            },
            separatorBuilder: (context, index) {
              return SizedBox(
                width: Dimens.MEDIUM_SPACE,
              );
            },
            itemCount: screenshots!.length),
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          TitleComponent(title: 'Screenshot'),
          SizedBox(height: Dimens.MAXX_SPACE,),
          _buildScreenshotListView()
        ],
      ),
    );
  }
}
