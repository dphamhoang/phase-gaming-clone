import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/models/media_video.dart';
import 'package:phase_gaming/src/ui/components/shared/video_view.dart';

class GameDetailVideo extends StatelessWidget {
  final Game game;
  const GameDetailVideo({Key? key, required this.game}) : super(key: key);

  Widget _buildGameDetailVideo() {
    List<MediaVideo> listGameVideo = <MediaVideo>[];
    if (game.trailers != null) {
      if (game.trailers!.length > 0) {
        listGameVideo.addAll(game.trailers!);
      }
    }
    if (game.gamePlayClips != null) {
      if (game.gamePlayClips!.length > 0) {
        listGameVideo.addAll(game.gamePlayClips!);
      }
    }

    return ListView.separated(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          if (listGameVideo.length > 0) {
            return VideoView(
                videoUrl: listGameVideo[index].x1920x1080 != null
                    ? listGameVideo[index].x1920x1080!
                    : listGameVideo[index].link!);
          }
          return SizedBox();
        },
        separatorBuilder: (context, index) {
          return SizedBox(
            width: Dimens.MEDIUM_SPACE,
          );
        },
        itemCount: listGameVideo.length);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: Dimens.MEDIUM_SPACE),
      height: Dimens.GAME_ITEM_HEIGHT,
      child: _buildGameDetailVideo(),
    );
  }
}
