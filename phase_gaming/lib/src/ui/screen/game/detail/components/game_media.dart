import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/components/shared/video_view.dart';

class GameMedia extends StatelessWidget {
  final Game game;
  const GameMedia({Key? key, required this.game}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          top: Dimens.MAXX_SPACE,
          bottom: Dimens.MAXX_SPACE,
          right: Dimens.MEDIUM_SPACE),
      child: Wrap(
        children: [
          if (game.trailers != null)
            if (game.trailers!.length > 0)
              VideoView(
                  videoUrl: game.trailers![0].link == null
                      ? game.trailers![0].x1920x1080!
                      : game.trailers![0].link!)
            else if (game.gamePlayClips != null)
              if (game.gamePlayClips!.length > 0)
                VideoView(
                    videoUrl: game.gamePlayClips![0].link == null
                        ? game.gamePlayClips![0].x1920x1080!
                        : game.gamePlayClips![0].link!)
        ],
      ),
    );
  }
}
