import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/game.dart';

class MainInfo extends StatelessWidget {
  final Game game;
  final Size size;
  const MainInfo({Key? key, required this.game, required this.size})
      : super(key: key);

  Widget _buildGameDetailTile() {
    return Container(
      height: Dimens.GAME_INFO_ITEM_HEIGHT - 5,
      width: Dimens.SMALL_WIDTH_IMG,
      margin: EdgeInsets.only(right: Dimens.MEDIUM_SPACE),
      decoration:
          BoxDecoration(image: DecorationImage(image: NetworkImage(game.tile))),
    );
  }

  Widget _buildListGameTag() {
    return Container(
      height: Dimens.CATEGORY_HEIGHT + 4,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  border: Border.all(width: 0.3, color: Colors.white),
                  borderRadius: BorderRadius.all(
                      const Radius.circular(Dimens.MAX_RADIUS))),
              child: Center(
                child: Text(
                  game.tags[index].tagName,
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.5),
                      fontWeight: FontWeight.w100),
                ),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return SizedBox(
              width: Dimens.MEDIUM_SPACE + 2,
            );
          },
          itemCount: game.tags.length),
    );
  }

  Widget _buildGameRating() {
    return RatingBar.builder(
        initialRating: game.rating!,
        minRating: 0,
        maxRating: 5,
        itemCount: 5,
        itemSize: 20,
        itemPadding: EdgeInsets.symmetric(horizontal: 2),
        itemBuilder: (context, _) => Icon(
              Icons.star_outlined,
              color: AppColors.colorPrimary,
            ),
        onRatingUpdate: (rating) {});
  }

  Widget _buildPayTypeInfo(BuildContext context) {
    return FittedBox(
      child: Container(
        margin: EdgeInsets.only(top: 20),
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            border: Border.all(width: 0.5, color: Colors.white),
            borderRadius: BorderRadius.all(const Radius.circular(10))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              game.payTypeName,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            Text(
              'Free game for Member\nClick and play, no account needed',
              style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _buildGameDetailTile(),
          Expanded(
              child: Container(
            height: Dimens.GAME_INFO_ITEM_HEIGHT - 5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: size.width / 2,
                  child: Text(
                    game.name,
                    maxLines: 2,
                    overflow: TextOverflow.clip,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
                _buildGameRating(),
                _buildListGameTag(),
                Spacer(),
                _buildPayTypeInfo(context)
              ],
            ),
          ))
        ],
      ),
    );
  }
}
