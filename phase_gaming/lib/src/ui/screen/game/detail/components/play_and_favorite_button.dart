import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';

class PlayAndFavoriteButton extends StatelessWidget {
  const PlayAndFavoriteButton({Key? key}) : super(key: key);

  Widget _buildPlayBtn(BuildContext context) {
    return Flexible(
        flex: 2,
        child: GestureDetector(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: Dimens.MAX_SPACE),
            decoration: BoxDecoration(
                color: AppColors.colorPrimary,
                borderRadius: BorderRadius.all(
                    const Radius.circular(Dimens.MAXX_RADIUS))),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                  Icons.play_circle,
                  color: Colors.black,
                ),
                Container(
                  height: Dimens.CATEGORY_HEIGHT + 10,
                  alignment: Alignment.center,
                  child: Text(
                    'Play',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                Container()
              ],
            ),
          ),
        ));
  }

  Widget _buildFavoriteBtn() {
    return Container(
      margin:
          EdgeInsets.only(left: Dimens.MAX_SPACE, right: Dimens.NORMAL_SPACE),
      child: IconButton(
        onPressed: () {},
        icon: SvgPicture.asset(
          'assets/icon/favorite.svg',
          currentColor: Colors.white,
        ),
        padding: EdgeInsets.all(0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          left: Dimens.MEDIUM_SPACE, right: Dimens.MEDIUM_SPACE),
      margin:
          EdgeInsets.only(top: Dimens.MAXX_SPACE, bottom: Dimens.MAXXXX_SPACE),
      child: Row(
        children: [_buildPlayBtn(context), _buildFavoriteBtn()],
      ),
    );
  }
}
