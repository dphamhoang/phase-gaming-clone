import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:phase_gaming/src/blocs/game/game_bloc.dart';
import 'package:phase_gaming/src/blocs/game/game_event.dart';
import 'package:phase_gaming/src/blocs/game/game_state.dart';
import 'package:phase_gaming/src/ui/navigation/app_route.dart';
import 'package:phase_gaming/src/ui/screen/game/detail/components/body.dart';

class GameDetail extends StatefulWidget {
  static MaterialPage page(int gameId) {
    return MaterialPage(
        name: AppPath.gameDetailPath,
        key: ValueKey(AppPath.gameDetailPath),
        child: GameDetail(
          gameId: gameId,
        ));
  }

  final int gameId;
  bool isExpanded = false;
  GameDetail({required this.gameId});
  @override
  State<StatefulWidget> createState() => _GameDetailState();
}

class _GameDetailState extends State<GameDetail> {
  late GameBloc gameBloc;

  @override
  void initState() {
    gameBloc = GameBloc(GameState());
    gameBloc.add(GetGameById(id: widget.gameId));
    super.initState();
  }

  Widget _buildBody(Size size) {
    return BlocBuilder<GameBloc, GameState>(
      builder: (context, state) {
        if (state.game == null) {
          if (Platform.isIOS) {
            return Center(
              child: CupertinoActivityIndicator(),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return Body(game: state.game!);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: BlocProvider(
        create: (_) => gameBloc,
        child: _buildBody(size),
      ),
    );
  }
}
