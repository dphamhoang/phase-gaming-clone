import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/blocs/home/home_state.dart';
import 'package:phase_gaming/src/ui/screen/home/components/home_banner.dart';
import 'package:phase_gaming/src/ui/screen/home/components/home_header.dart';
import 'package:phase_gaming/src/ui/screen/home/components/tags.dart';

import 'home_list.dart';

class Body extends StatelessWidget {
  final HomeState state;
  final Future Function() onRefresh;
  Body({Key? key, required this.state, required this.onRefresh})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                HomeHeader(),
                HomeBanner(banners: state.banners),
                SizedBox(
                  height: Dimens.MAXX_SPACE,
                ),
                Tags(tags: state.tags),
                SizedBox(
                  height: Dimens.MAXX_SPACE,
                ),
                HomeList(state: state),
              ],
            ),
          ),
        ),
        onRefresh: onRefresh);
  }
}
