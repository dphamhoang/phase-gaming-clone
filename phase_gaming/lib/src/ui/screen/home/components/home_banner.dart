import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/models/banner.dart';

class HomeBanner extends StatelessWidget {
  final List<AppBanner> banners;
  HomeBanner({Key? key, required this.banners}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
        itemCount: banners.length,
        itemBuilder: (context, index, pageViewIndex) {
          return Container(
            width: MediaQuery.of(context).size.width,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(const Radius.circular(Dimens.MAX_RADIUS)),
              child: Image.network(
                banners[index].link,
                fit: BoxFit.cover,
              ),
            ),
          );
        },
        options: CarouselOptions(
            height: 200,
            initialPage: 0,
            viewportFraction: 0.85,
            enableInfiniteScroll: false,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 2),
            autoPlayCurve: Curves.linearToEaseOut,
            scrollDirection: Axis.horizontal,
            enlargeCenterPage: true,
            enlargeStrategy: CenterPageEnlargeStrategy.scale));
  }
}
