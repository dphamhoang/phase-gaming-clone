import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20, top: Dimens.MAXXXX_SPACE),
      padding: EdgeInsets.only(left: Dimens.MAX_SPACE),
      child: Text(
        'Phase',
        style: Theme.of(context).textTheme.headline1,
      ),
    );
  }
}
