import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';
import 'package:phase_gaming/src/assets/values/list_style.dart';
import 'package:phase_gaming/src/blocs/home/home_state.dart';
import 'package:phase_gaming/src/resources/constant/constant.dart';
import 'package:phase_gaming/src/ui/components/home/list-game/list_game_with_title.dart';

class HomeList extends StatelessWidget {
  final HomeState state;
  HomeList({Key? key, required this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ListGameWithTitle(
              games: state.newGame,
              title: Constant.newGame,
              itemStyle: ItemStyle.portrait,
              itemSize: ItemSize.portrait,
              listDirection: ListDirection.horizontal),
          SizedBox(
            height: Dimens.MAX_SPACE,
          ),
          ListGameWithTitle(
              games: state.trendingGame,
              title: Constant.trendingGame,
              itemStyle: ItemStyle.portrait,
              itemSize: ItemSize.smallPortrait,
              listDirection: ListDirection.horizontal),
          SizedBox(
            height: Dimens.MAX_SPACE,
          ),
          ListGameWithTitle(
              games: state.hotGame,
              title: Constant.hotGame,
              itemStyle: ItemStyle.portrait,
              itemSize: ItemSize.smallPortrait,
              listDirection: ListDirection.horizontal),
          SizedBox(
            height: Dimens.MAX_SPACE,
          ),
          ListGameWithTitle(
              games: state.editorChoice,
              title: Constant.editorChoice,
              itemStyle: ItemStyle.landscape,
              itemSize: ItemSize.smallPortrait,
              listDirection: ListDirection.horizontal),
          SizedBox(
            height: Dimens.MAX_SPACE,
          ),
        ],
      ),
    );
  }
}
