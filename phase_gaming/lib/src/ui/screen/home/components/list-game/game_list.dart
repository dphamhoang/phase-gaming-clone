import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/list_style.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/components/home/list-game/list_game.dart';

class GameList extends StatelessWidget {
  final List<Game> games;
  final ItemSize itemSize;
  final ItemStyle itemStyle;
  final ListDirection listDirection;
  GameList(
      {Key? key,
      required this.games,
      required this.itemSize,
      required this.itemStyle,
      required this.listDirection})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListGame(
        games: games,
        itemStyle: itemStyle,
        itemSize: itemSize,
        listDirection: listDirection);
  }
}
