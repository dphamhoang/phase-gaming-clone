import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/list_style.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/components/home/list-game/list_game_with_title.dart';

class GameListWithTitle extends StatelessWidget {
  final List<Game> games;
  final String title;
  final ItemSize? itemSize;
  final ItemStyle itemStyle;
  final ListDirection listDirection;

  GameListWithTitle(
      {Key? key,
      required this.games,
      required this.title,
      required this.itemStyle,
      this.itemSize,
      required this.listDirection})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListGameWithTitle(
      games: games,
      title: title,
      itemStyle: itemStyle,
      itemSize: itemSize,
      listDirection: listDirection,
    );
  }
}
