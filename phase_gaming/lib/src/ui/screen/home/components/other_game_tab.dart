import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/list_style.dart';
import 'package:phase_gaming/src/models/game.dart';
import 'package:phase_gaming/src/ui/screen/home/components/list-game/game_list.dart';

class OtherGameTab extends StatelessWidget {
  final List<Game> games;
  final bool _isLogged = false;

  OtherGameTab({Key? key, required this.games}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.maxFinite,
      child: DefaultTabController(
          length: _isLogged ? 2 : 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: _isLogged
                    ? MediaQuery.of(context).size.width / 1.45
                    : MediaQuery.of(context).size.width / 3,
                child: TabBar(
                  indicatorColor: AppColors.colorPrimary,
                  labelColor: AppColors.colorPrimary,
                  unselectedLabelColor: Colors.white,
                  tabs: [
                    Tab(text: 'FREE TO PLAY'),
                    if (_isLogged)
                      Tab(
                        text: 'RECOMMENDED',
                      )
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height,
                child: TabBarView(physics: BouncingScrollPhysics(), children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    child: GameList(
                      games: games,
                      itemSize: ItemSize.landscape,
                      itemStyle: ItemStyle.vertical,
                      listDirection: ListDirection.vertical,
                    ),
                  ),
                  if (_isLogged)
                    Container(
                      child: Center(
                        child: Text('Recommended tab'),
                      ),
                    )
                ]),
              )
            ],
          )),
    );
  }
}
