import 'package:flutter/material.dart';
import 'package:phase_gaming/src/models/tag.dart';
import 'package:phase_gaming/src/ui/components/tags/tag_component.dart';

class Tags extends StatelessWidget {
  final List<Tag> tags;
  Tags({Key? key, required this.tags}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TagsComponent(tags: tags);
  }
}
