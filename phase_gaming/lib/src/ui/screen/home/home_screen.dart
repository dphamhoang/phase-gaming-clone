import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:phase_gaming/src/blocs/home/home_bloc.dart';
import 'package:phase_gaming/src/blocs/home/home_event.dart';
import 'package:phase_gaming/src/blocs/home/home_state.dart';
import 'package:phase_gaming/src/ui/components/loading.dart';
import 'package:phase_gaming/src/ui/screen/home/components/body.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeBloc homeBloc;
  bool _isLogged = false;
  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc(HomeState());
    homeBloc.add(GetHomeData());
    _checkUserIsLogged();
  }

  @override
  void dispose() {
    homeBloc.close();
    super.dispose();
  }

  Future<void> _checkUserIsLogged() async {
    final _storage = FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    if (token != null) {
      _isLogged = true;
    }
  }

  Future<void> _refresh() async {
    homeBloc.add(GetHomeData());
  }

  Widget _buildBody(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        if (state.isLoading) {
          return Loading();
        }
        return Body(state: state, onRefresh: _refresh);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(create: (_) => homeBloc, child: _buildBody(context));
  }
}
