import 'package:flutter/material.dart';
import 'package:phase_gaming/src/assets/values/colors.dart';
import 'package:phase_gaming/src/assets/values/dimens.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
      padding: EdgeInsets.all(Dimens.NORMAL_SPACE),
      child: Column(
        children: [
          buildUserInfo(),
        ],
      ),
    ));
  }

  Widget buildUserInfo() {
    return Container(
        alignment: Alignment.topCenter,
        // color: Colors.green,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildAvatar(),
            Flexible(
              child: Padding(
                padding: EdgeInsets.all(Dimens.NORMAL_SPACE),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("this_is_a_longggggggggggggggggg_email@gmail.com",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(backgroundColor: Colors.blue)),
                    Text("0123456789")
                  ],
                ),
              )
            )
          ],
        )
    );
  }

  Widget buildAvatar() {
    return Container(
        width: MediaQuery.of(context).size.width * 0.35,
        height: MediaQuery.of(context).size.width * 0.35,
        child: CircleAvatar(
          backgroundColor: AppColors.colorPrimary,
          child: FractionallySizedBox(
            widthFactor: 0.98,
            heightFactor: 0.98,
            child: CircleAvatar(
              backgroundImage:
                  NetworkImage('https://googleflutter.com/sample_image.jpg'),
            ),
          ),
        ));
  }
}
