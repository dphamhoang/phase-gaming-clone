import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:phase_gaming/src/blocs/app/app_bloc.dart';
import 'package:phase_gaming/src/blocs/app/app_event.dart';
import 'package:phase_gaming/src/ui/navigation/app_route.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);
  static MaterialPage page() => MaterialPage(
      name: AppPath.splashScreenPath,
      key: ValueKey(AppPath.splashScreenPath),
      child: SplashScreen());

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    Future.delayed(const Duration(seconds: 2), () {
      BlocProvider.of<AppBloc>(context).add(InitializeEvent());
    });
    BlocProvider.of<AppBloc>(context).add(LoggedInEvent());
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
            child: SizedBox(
          child: SvgPicture.asset('assets/image/logo.svg'),
        )),
      ),
    );
  }
}
