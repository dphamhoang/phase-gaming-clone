import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:phase_gaming/src/models/base_response.dart';

class JsonHandler extends JsonConverter {
  Map<Type, Function> fromJson;
  JsonHandler(this.fromJson);

  @override
  Response<BodyType> convertResponse<BodyType, InnerType>(Response response) {
    // TODO: implement convertResponse
    return response.copyWith(
        body: fromJsonData<BodyType, InnerType>(
            response.body, fromJson[InnerType]));
  }

  T fromJsonData<T, InnerType>(String jsonData, Function? fromJson) {
    var jsonDecode = json.decode(jsonData);
    BaseResponse response = BaseResponse.fromJson(jsonDecode);
    if (response.statusCode == 200) {
      final data = response.data;
      if (data is List) {
        return data
            .map((item) => fromJson!(item as Map<String, dynamic>) as InnerType)
            .toList() as T;
      }
      return fromJson!(data) as T;
    } else {
      return response.message as T;
    }
  }

  @override
  Request convertRequest(Request request) {
    // TODO: implement convertRequest
    return super.convertRequest(request);
  }
}
