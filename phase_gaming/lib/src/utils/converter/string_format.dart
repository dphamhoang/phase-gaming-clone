import 'package:html/parser.dart';
import 'package:phase_gaming/src/models/tag.dart';

class StringFormat {
  static String parseHtml(String html) {
    final document = parse(html);
    final String parsedString =
        parse(document.body!.text).documentElement!.text;
    return parsedString;
  }

  static String itemVerticalTagFormat(List<Tag> tags) {
    String formatTag = "";
    tags.forEach((element) {
      final tagFormat = '#${element.tagName} ';
      formatTag += tagFormat;
    });
    return formatTag;
  }
}
